// ============================================================================
//
// = CONTEXT
//    TANGO Project -
//
// = FILENAME
//    FpsTaskManager.hpp
//
// = AUTHOR
//    F. Thiam
//
// ============================================================================

#ifndef _MY_DEVICE_TASK_H_
#define _MY_DEVICE_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
//#include <yat/any/GenericContainer.h>
#include <yat4tango/DeviceTask.h>
#include <yat/utils/XString.h>
#include "fps3010.h"
#include <list>
const int MAX_LENGTH = 100000;

// ============================================================================
// class: FpsTaskManager
// ============================================================================
class FpsTaskManager : public yat4tango::DeviceTask
{
public:

    //- Initialization 
	FpsTaskManager (Tango::DeviceImpl * devHost, int id);

    //- the Tango device hosting this task
    Tango::DeviceImpl * _hostDev;
  
	//- dtor
    virtual ~FpsTaskManager (void);

    void startAcquisition(unsigned int sampleQuantity, unsigned int lbSmpTime);
	void startLiveAcquisition(unsigned int averageTime, int bufferSize);
	void stopLiveAcquisition();
    void stopAcquisition();
	void resetAxes();
	void resetAxis(unsigned int axisNo);
	void startAdjustment();
	int GetFeatures();
	
	
	         
	typedef struct {
		double positionsBufferedV1[MAX_LENGTH];
		double positionsBufferedV2[MAX_LENGTH];
		double positionsBufferedV3[MAX_LENGTH];
		double sampleQuantityBuffered;
	}dataPacket;

	typedef struct {
		std::list<double> positionV1;
		std::list<double> positionV2;
		std::list<double> positionV3;
	}currentRecord;

	typedef struct {
		double temperature;
		double pression;
		double hygrometry;
		double refraction;
	}EcuData;

	typedef struct {
		bool axis1Error;
		bool axis2Error;
		bool axis3Error;
		bool axis1Valid;
		bool axis2Valid;
		bool axis3Valid;
		bool adjustment;
		bool alignment;
	}axesState;

	typedef struct{
		bool adjust;
		bool align;
	}deviceState;

	
	bool synchronousDataToImport();
    void synchronousDataImported();
    std::string getErrorFromFps();
	bool getLiveStatus();

	//To get the values from device
    double *getBufferedPositions(short select);
	double getSampleQuantityBuffered();
	
	axesState getAxesStates();
	deviceState getDeviceStates();
	std::list<double> getLiveRecording(short select);
	EcuData getEcuData();
	
protected:
	//- process_message (implements yat4tango::DeviceTask pure virtual method)
	virtual void process_message (yat::Message& msg)
    throw (Tango::DevFailed);
		
	//Mutex for data access//dll
	yat::Mutex _data_mutex;
	yat::Mutex _Fps_Access;

private:
	//Connection 	
    unsigned int _devNo;
    std::string _addr;
	int _deviceId;
    bool _connected;
	bool _liveModeOn;
	int _liveBufferSize;
	//Config
	unsigned int _lbSmpTime;
	unsigned int _liveSmpTime;
	int _features;

	//State
	void refreshAxesStates();	
	axesState _axesCurrentState;
	deviceState _deviceCurrentState;
	std::string _errorFromFps; 
	 
	//Data
    bool _synchronousDataToImport;	
	dataPacket _dataToReturn;
	std::string getMessage(int code);
	currentRecord _liveRecord;
	EcuData _ecuData;
	
	//Fps access
	void fpsAxisState(bool *axisErrorState, bool *axisValidState, unsigned int axisNo);
	void fpsRefreshDeviceState();
	void fpsAxesPositions();
	void fpsStartAdjustement();
	void fpsResetAxes();
	void fpsResetAxis(unsigned int axisNo);
	void fpsSetPositionCallback(unsigned int lbSmpTime);
	short fpsSelectDevice();
	void fpsSetPosAverage(unsigned int axisNo, unsigned int sampleTime);
	int fpsGetPosAverage(unsigned int axisNo);
	void fpsGetEcuData();
	int fpsCheckFeatures();
	
	
   
};

#endif // _MY_DEVICE_TASK_H_
