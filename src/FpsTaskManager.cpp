// ============================================================================
//
// = CONTEXT
//    TANGO Project -
//
// = FILENAME
//    LeemInterface.cpp
//
// = AUTHOR
//    F. Thiam
//
// ============================================================================
// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "FpsTaskManager.h"
#define SAMPLE_TIME_UNIT 1.024e-5  

//Variables for position Callback (local function) 
double posFromCallBack[3][MAX_LENGTH];
unsigned int sampleQuantity;
bool dataIsReady;
bool readyToRecordData;
unsigned int totalNumberOfSamples;
unsigned int axis1 = 0;
unsigned int axis2 = 1;
unsigned int axis3 = 2;
unsigned int OldlbSmpTime = 0; 
int userUnit = 1000000;

static const unsigned short kPERIODIC_TMOUT_MS = 10;
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
#define kCONFIGURE_ACQUISITION  		(yat::FIRST_USER_MSG + 1006)
#define kCONFIGURE_LIVE_ACQUISITION		(yat::FIRST_USER_MSG + 1007)
#define kSET_RECORD_FROM_CALLBACK		(yat::FIRST_USER_MSG + 1008)
#define kENOUGH_DATA_BUFFERED  			(yat::FIRST_USER_MSG + 1009)
// ============================================================================
// FpsTaskManager::FpsTaskManager
// ============================================================================
FpsTaskManager::FpsTaskManager (Tango::DeviceImpl * host_dev, int id)
:	yat4tango::DeviceTask(host_dev),
_hostDev(host_dev)
{
    //Initialize member variables
	yat::MutexLock gardDataMutex(this->_data_mutex);
	yat::MutexLock gardFpsAccessMutex(this->_Fps_Access);
    this->_synchronousDataToImport = false;
	this->_deviceId = id;
    this->_devNo = 0;
    this->_addr = "";
    this->_errorFromFps = "";
    this->_connected = false;
	this->_liveModeOn = false;
	this->_features = 0;
	OldlbSmpTime = 0;

	this->_liveBufferSize = 0;
	this->_axesCurrentState.axis1Error = false;
	this->_axesCurrentState.axis2Error = false;
	this->_axesCurrentState.axis3Error = false;
	this->_axesCurrentState.axis1Valid = false;
	this->_axesCurrentState.axis2Valid = false;
	this->_axesCurrentState.axis3Valid = false;
	this->_deviceCurrentState.adjust = false;
	this->_deviceCurrentState.align = false;
}//release Data/Dll access mutex
// ============================================================================
// FpsTaskManager::~FpsTaskManager
// ============================================================================
FpsTaskManager::~FpsTaskManager ()
{
	this->enable_periodic_msg(false);
	this->_connected = false;
	yat::MutexLock gard(this->_Fps_Access);
	FPS_disconnect(this->_devNo);

}
// ============================================================================
// FpsTaskManager::positionCallback
//
// This function is a callback function, wich is why we need to declare it as 
// a local function.
// ============================================================================
static void positionCallback( unsigned int devNo, unsigned int count, unsigned int index, const double * const pos[3])
{
    //positionCallBack is called by the device when it has to return values
    //packages dimensions are different, depends on lbSampletime
    //Once the first aqcusition is started, position callBack is always called back by the device
    //tests shows us that if it's stopped by sending FPS_fpsSetPositionCallback(devNo, NULL, lbSampleTime)
    //it can't be started again, so we're using this thread to deal with it...
    if (readyToRecordData)
    {
        //To check if sampling is over.		
		if (totalNumberOfSamples < sampleQuantity)
		{
			unsigned int numberOfSampleToRecordFromCallback;
			
	        if (totalNumberOfSamples + count <= sampleQuantity) 
				numberOfSampleToRecordFromCallback = count;

			else  if (totalNumberOfSamples + count >= sampleQuantity ) 
				numberOfSampleToRecordFromCallback = sampleQuantity - totalNumberOfSamples;
			//then we store the data
			for (unsigned int n = 0; n < numberOfSampleToRecordFromCallback; n++)
			{
				posFromCallBack[0][totalNumberOfSamples + n ] = pos[0][n]/userUnit;
				posFromCallBack[1][totalNumberOfSamples + n ] = pos[1][n]/userUnit;
				posFromCallBack[2][totalNumberOfSamples + n ] = pos[2][n]/userUnit;
			} 
			totalNumberOfSamples = totalNumberOfSamples + numberOfSampleToRecordFromCallback;	
		}
        //else acquisition is over
        else
        {
            dataIsReady = true;
            readyToRecordData = false;  
        }
    } 
}

// ============================================================================
// FpsTaskManager::process_message
// ============================================================================
void FpsTaskManager::process_message (yat::Message& _msg)
  throw (Tango::DevFailed)
{
    //DEBUG_STREAM << "FpsTaskManager::process_message::receiving msg " <<_msg.to_string() << std::endl;
//- handle msg
switch (_msg.type())
{
     //- TASK_PERIODIC ------------------
    case yat::TASK_PERIODIC:
    {
        DEBUG_STREAM << "FpsTaskManager::handle_message::TASK_PERIODIC" << std::endl;
        // to detect if there is enough data buffered
        if (dataIsReady)
        {
            //then post the message to send data
            this->post(kENOUGH_DATA_BUFFERED);
            dataIsReady = false;
        }
		refreshAxesStates();
		fpsRefreshDeviceState();
		//if live mode on, then record data continuously		
		if (this->_liveModeOn)
			fpsAxesPositions();		
		if (this->_features == FPS_FeatureEcu)	
			fpsGetEcuData();
			
    }
    break;

    //- TASK_INIT ----------------------
    case yat::TASK_INIT:
    {
        DEBUG_STREAM << "FpsTaskManager::handle_message::TASK_INIT::thread is starting up" << std::endl;
        // To initialize the periodic task
		this->_connected =false;
        this->enable_timeout_msg(false);
        this->set_timeout_msg_period (1000);
        this->set_periodic_msg_period(kPERIODIC_TMOUT_MS);
        this->enable_periodic_msg(false);  
       
        // To connect the device       
        short response = fpsSelectDevice();
        if (response == 0)
        {
            this->enable_periodic_msg(true);	
			this->_features = this->fpsCheckFeatures();
        }
        else 
        {
			//Dll access : LockMutex
			yat::MutexLock gard(this->_Fps_Access);
            if (response == -1)
			this->_errorFromFps = "FPS3010 Not connected";
            else if (response == 1)
			{
				stringstream errorMessageStream;
				errorMessageStream<<"could not connect to FPS3010 with ID : "<<_deviceId;
				this->_errorFromFps = errorMessageStream.str();
			}
        }
    } 
    break;

    //- TASK_EXIT ----------------------
    case yat::TASK_EXIT:
    {
        DEBUG_STREAM << "FpsTaskManager::handle_message::TASK_EXIT::thread is quitting" << std::endl;
    }
    break;

    //- THREAD_TIMEOUT -------------------
    case yat::TASK_TIMEOUT:
    {
    
    }
    break;
	
	case kCONFIGURE_ACQUISITION:
    {
        DEBUG_STREAM << "FpsTaskManager::handle_message::kCONFIGURE_ACQUISITION" << std::endl;
		//TO SET SAMPLE TIME - Starts the callback function
		//if sample Time has changed we have to wait 1s for the callBack function to return the right frequency packets...
		if(OldlbSmpTime != this->_lbSmpTime)
		{
			this->fpsSetPositionCallback(this->_lbSmpTime);
			Sleep(1000);
		}
		else
			this->fpsSetPositionCallback(this->_lbSmpTime);
		//Set old Sample time for next configuration
		OldlbSmpTime = this->_lbSmpTime;
		_liveModeOn = false;
		//Configuration is ready
		this->post(kSET_RECORD_FROM_CALLBACK);
    }
	break;
		
	case kCONFIGURE_LIVE_ACQUISITION:
    {
        DEBUG_STREAM << "FpsTaskManager::handle_message::kCONFIGURE_ACQUISITION" << std::endl;
	
		//To set sample time acquisition on the 3 axes.. (doesn't work)		
		this->fpsSetPosAverage (axis1, _liveSmpTime);
		this->fpsSetPosAverage (axis2, _liveSmpTime);
		this->fpsSetPosAverage (axis3, _liveSmpTime);
		this->_synchronousDataToImport = false;
		this->_liveModeOn = true;
    }
	break;	

    //- kSET_RECORD_FROM_CALLBACK ---------------
    case kSET_RECORD_FROM_CALLBACK:
    {
        DEBUG_STREAM << "FpsTaskManager::handle_message::kSET_RECORD_FROM_CALLBACK" << std::endl;
        // To record data from fps callBack
        readyToRecordData = true;
    }
    break;

    //- kENOUGH_DATA_BUFFERED ---------------
    case kENOUGH_DATA_BUFFERED:
    {
        DEBUG_STREAM << "FpsTaskManager::handle_message::kENOUGH_DATA_BUFFERED" << std::endl;
		//Data access : LockMutex
		yat::MutexLock gard(this->_data_mutex);
		 // Prepare data for device in data to return (data packet type)
        this->_dataToReturn.sampleQuantityBuffered = totalNumberOfSamples;       
        for (unsigned int i = 0; i < totalNumberOfSamples; ++i ) 
        {
			this->_dataToReturn.positionsBufferedV1[i] = posFromCallBack[0][i];
			this->_dataToReturn.positionsBufferedV2[i] = posFromCallBack[1][i];
			this->_dataToReturn.positionsBufferedV3[i] = posFromCallBack[2][i];
        }
		 // To let the device know that samples are ready to be read
        this->_synchronousDataToImport = true;
    }//release data-accessMutex
    break;

    //- UNHANDLED MSG --------------------
    default:
	
      DEBUG_STREAM << "FpsTaskManager::handle_message::unhandled msg type received" << std::endl;
	    break;
    }
}
//-----------------------------------------------------------------------------------------------
//
//
//				COMANDS from device
//
//
//-----------------------------------------------------------------------------------------------
// ============================================================================
// FpsTaskManager::startAcquisition
// ============================================================================
void FpsTaskManager::startAcquisition(unsigned int sampleQuantitySet, unsigned int lbSmpTime)
{
	// SampleQuantity is used by callback function
    sampleQuantity = sampleQuantitySet;
    totalNumberOfSamples = 0;
	//Flush Values
	this->_synchronousDataToImport = false;
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	for (unsigned int i = 0; i < MAX_LENGTH; ++i ) 
    {
        this->_dataToReturn.positionsBufferedV1[i] = 0;
        this->_dataToReturn.positionsBufferedV2[i] = 0;
        this->_dataToReturn.positionsBufferedV3[i] = 0;
		posFromCallBack[0][i] = 0;
		posFromCallBack[1][i] = 0;
		posFromCallBack[2][i] = 0;
    }
    this->_dataToReturn.sampleQuantityBuffered = 0;
	this->_lbSmpTime = lbSmpTime;
	
	this->post(kCONFIGURE_ACQUISITION);
}//release data-accessMutex
// ============================================================================
// FpsTaskManager::stopAcquisition
// ============================================================================
void FpsTaskManager::stopAcquisition()
{
	//If we were recording data then we stop acquisition
	if (readyToRecordData)
	{
		//if we were in classique mode
	    dataIsReady = true;
	    readyToRecordData = false;
	}
}
// ============================================================================
// FpsTaskManager::startLiveAcquisition
// ============================================================================
void FpsTaskManager::startLiveAcquisition(unsigned int averageTime, int bufferSize)
{
	this->_liveSmpTime = averageTime;
	this->_liveBufferSize = bufferSize;
	this->post(kCONFIGURE_LIVE_ACQUISITION);
}
// ============================================================================
// FpsTaskManager::stopLiveAcquisition
// ============================================================================
void FpsTaskManager::stopLiveAcquisition()
{
	if(this->_liveModeOn)
	{
		//flush data 
		this->_liveRecord.positionV1.clear();
		this->_liveRecord.positionV2.clear();
		this->_liveRecord.positionV3.clear();
		this->_liveModeOn = false;
	}
}
// ============================================================================
// FpsTaskManager::getLiveStatus
// ============================================================================
bool FpsTaskManager::getLiveStatus()
{
	return this->_liveModeOn;
}
// ============================================================================
// FpsTaskManager::synchronousDataToImport
// To check if device can get data
// ============================================================================ 
bool FpsTaskManager::synchronousDataToImport()
{
    return this->_synchronousDataToImport;
}
// ============================================================================
// FpsTaskManager::synchronousDataImported
// Data has been imported by device
// ============================================================================ 
void FpsTaskManager::synchronousDataImported()
{
    this->_synchronousDataToImport = false;
}
// ============================================================================
// FpsTaskManager::resetAxes
// ============================================================================
void FpsTaskManager::resetAxes()
{
	this->fpsResetAxes();
}
// ============================================================================
// FpsTaskManager::resetAxis
// ============================================================================
void FpsTaskManager::resetAxis(unsigned int axisNo)
{
	this->fpsResetAxis(axisNo);
}
// ============================================================================
// FpsTaskManager::startAdjustment
// ============================================================================
void FpsTaskManager::startAdjustment()
{
	this->fpsStartAdjustement();
}
// ============================================================================
// FpsTaskManager::GetFeatures
// ============================================================================ 
int FpsTaskManager::GetFeatures()
{
	//Dll access : LockMutex
	yat::MutexLock gard(this->_Fps_Access);
    return this->_features;
}
// ============================================================================
// FpsTaskManager::getErrorFromFps
// ============================================================================ 
std::string FpsTaskManager::getErrorFromFps()
{
	//Dll access : LockMutex
	yat::MutexLock gard(this->_Fps_Access);
    return this->_errorFromFps;
}
// ============================================================================
// FpsTaskManager::getBufferedPositions
// ============================================================================ 
double *FpsTaskManager::getBufferedPositions(short select)
{
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	//To return selected buffer 
	if (select == 1)
		return this->_dataToReturn.positionsBufferedV1;
	if (select == 2)
		return this->_dataToReturn.positionsBufferedV2;
	if (select == 3)
		return this->_dataToReturn.positionsBufferedV3;
}
// ============================================================================
// FpsTaskManager::getLiveRecording
// ============================================================================ 
std::list<double> FpsTaskManager::getLiveRecording(short select)
{
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	//To return selected buffer 
	if (select == 1)
		return this->_liveRecord.positionV1;
	if (select == 2)
		return this->_liveRecord.positionV2;
	if (select == 3)
		return this->_liveRecord.positionV3;
}
// ============================================================================
// FpsTaskManager::getSampleQuantityBuffered
// ============================================================================ 
double FpsTaskManager::getSampleQuantityBuffered()
{
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	return this->_dataToReturn.sampleQuantityBuffered;
}
// ============================================================================
// FpsTaskManager::getEcuData
// ============================================================================ 
FpsTaskManager::EcuData FpsTaskManager::getEcuData()
{
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	return this->_ecuData;
}
// ============================================================================
// FpsTaskManager::getAxesStates
// to get state for each axis
// ============================================================================ 
FpsTaskManager::axesState FpsTaskManager::getAxesStates()
{
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	return this->_axesCurrentState;
}
// ============================================================================
// FpsTaskManager::getDeviceStates
// to get state of fps interferometer
// ============================================================================ 
FpsTaskManager::deviceState FpsTaskManager::getDeviceStates()
{
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	return this->_deviceCurrentState;
}
//-----------------------------------------------------------------------------------------------
//
//
//				Task use
//
//
//-----------------------------------------------------------------------------------------------
// ============================================================================
// FpsTaskManager::refreshAxesStates
// ============================================================================ 
void FpsTaskManager::refreshAxesStates()
{
	axesState axesCurrentState;	

	this->fpsAxisState(&axesCurrentState.axis1Error, &axesCurrentState.axis1Valid, axis1);
	this->fpsAxisState(&axesCurrentState.axis2Error, &axesCurrentState.axis2Valid, axis2);
	this->fpsAxisState(&axesCurrentState.axis3Error, &axesCurrentState.axis3Valid, axis3);
	
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	this->_axesCurrentState = axesCurrentState;

}//release data-accessMutex
// ============================================================================
// FpsTaskManager::getMessage
// ============================================================================
std::string FpsTaskManager::getMessage(int code)
{
  switch(code) {
  case FPS_Ok:           
      return "";
  case FPS_Error:        
      return "Unspecified error";
  case FPS_Timeout:      
      return "Communication timeout";
  case FPS_NotConnected: 
      return "No active connection to device";
  case FPS_DriverError:  
      return "Error in comunication with driver";
  case FPS_DeviceLocked: 
      return "Device is already in use by other";
  case FPS_Unknown:      
      return "Unknown error";
  case FPS_NoDevice:     
      return "Invalid device number in function call";
  default:               
      return "Unknown error code";
  }
 }
//-----------------------------------------------------------------------------------------------
//
//
//				FPS API Access
//
//
//-----------------------------------------------------------------------------------------------
// ============================================================================
// FpsTaskManager::fpsSelectDevice
// ============================================================================
short FpsTaskManager::fpsSelectDevice()
{
    unsigned int devCount = 0, devNo = 0;
	//Dll access : LockMutex
	yat::MutexLock gard(this->_Fps_Access);
    int rc = FPS_discover( IfAll, &devCount );   

    if ( devCount == 0 ) 
    {
        return -1;
    }
    // try to connect to each device discovered
    for ( devNo = 0; devNo < devCount; devNo++ ) 
    {
        int id = 0;
        char addr[16];
        int response_FPS_connect;
        rc = FPS_getDeviceInfo( devNo, &id, addr, 0 );
        getMessage(rc);
        this->_devNo = devNo;
        this->_addr = addr;
		//if  the id is good then connect
        if(id == this->_deviceId)
		{
	        response_FPS_connect = FPS_connect( this->_devNo );
	        if (response_FPS_connect==0)
	        {
				this->_connected = true;
				return 0;	
	        }
		}
    }
    // can't connect to any device discovered
    return 1;
}//release dll-accessMutex
// ============================================================================
// FpsTaskManager::fpsCheckFeatures
// ============================================================================ 
int FpsTaskManager::fpsCheckFeatures()
{
    unsigned int axisCount;
    int features;
    
	if(this->_connected)
	{
		////Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		int rc = FPS_getDeviceConfig((unsigned int) this->_devNo, &axisCount, &features);
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}
	}
	return features;
}//release dll-accessMutex
// ============================================================================
// FpsTaskManager::fpsGetEcuData
// ============================================================================ 
void FpsTaskManager::fpsGetEcuData()
{	
	double temperature = 0;
	double pression = 0;
	double hygrometry = 0;
	double refraction = 0;
	if(this->_connected)
	{
		int rc = 0;
		////Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_getEcuData((unsigned int) this->_devNo, &temperature, &pression, &hygrometry, &refraction);
		
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}
	}//release dll-accessMutex
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	this->_ecuData.temperature = temperature;
	this->_ecuData.pression = pression;
	this->_ecuData.hygrometry = hygrometry;
	this->_ecuData.refraction = refraction;
		
}//release data-accessMutex
// ============================================================================
// FpsTaskManager::fpsSetPositionCallback
// ============================================================================ 
void FpsTaskManager::fpsSetPositionCallback(unsigned int lbSmpTime)
{
	if(this->_connected)
	{
		////Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		int rc = FPS_setPositionCallback((unsigned int) this->_devNo, (FPS_PositionCallback) positionCallback, lbSmpTime);
		std::string message = getMessage(rc);
		
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}
	}
}//release dll-accessMutex
// ============================================================================
// FpsTaskManager::fpsAxesPositions
// ============================================================================ 
void FpsTaskManager::fpsAxesPositions()
{	
	double positions[3];
	if(this->_connected)
	{
		int rc = 0;
		////Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_getPositions((unsigned int) this->_devNo, positions);
		
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}
	}//release dll-accessMutex
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	//If max size, delete first element
	if(this->_liveRecord.positionV1.size() >= _liveBufferSize)
		this->_liveRecord.positionV1.pop_front();
	if(this->_liveRecord.positionV2.size() >= _liveBufferSize)
		this->_liveRecord.positionV2.pop_front();
	if(this->_liveRecord.positionV3.size() >= _liveBufferSize)
		this->_liveRecord.positionV3.pop_front();	
	
	//std::cout<<"FpsTaskManager:: _liveRecord.positionV1.size() "<<_liveRecord.positionV1.size()<<std::endl;

	this->_liveRecord.positionV1.push_back(positions[0]/userUnit);
	this->_liveRecord.positionV2.push_back(positions[1]/userUnit);
	this->_liveRecord.positionV3.push_back(positions[2]/userUnit);

}//release data-accessMutex
// ============================================================================
// FpsTaskManager::fpsRefreshDeviceState
// ============================================================================ 
void FpsTaskManager::fpsRefreshDeviceState()
{	
	bln32 adjust = 0 ;
	bln32 align = 0;							   
	if(this->_connected)
	{	
		int rc = 0;
		////Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_getDeviceStatus((unsigned int) this->_devNo, &adjust, &align);
		
		/*std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}*/ // - Always being callled, can confuse device comportement
	}//release dll Access mutex
	
	//Data access : LockMutex
	yat::MutexLock gard(this->_data_mutex);
	this->_deviceCurrentState.adjust = (bool) adjust;
	this->_deviceCurrentState.align = (bool) align;
}//release data Access mutex

// ============================================================================
// FpsTaskManager::fpsAxisState
// ============================================================================ 
void FpsTaskManager::fpsAxisState(bool *axisErrorState, bool *axisValidState, unsigned int axisNo)
{
	bln32 axisError = 0 ;
	bln32 axisValid = 0;
	if(this->_connected)
	{
		int rc = 0;
		////Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_getAxisStatus((unsigned int) this->_devNo, axisNo, &axisValid, &axisError);
		
		/*std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}*/ // - Always being callled, can confuse device comportement
		
	}//release dll-accessMutex
	
	*axisErrorState = (bool) axisError;
	*axisValidState = (bool) axisValid;	
}//release data Access mutex
// ============================================================================
// FpsTaskManager::fpsStartAdjustement
// ============================================================================ 
void FpsTaskManager::fpsStartAdjustement()
{
	if(this->_connected)
	{	
		int rc = 0;
		//Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_startAdjustment(this->_devNo );
		
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}	
	}//release dll-accessMutex
}
// ============================================================================
// FpsTaskManager::fpsResetAxes
// ============================================================================ 
void FpsTaskManager::fpsResetAxes()
{
	if(this->_connected)
	{
		int rc = 0;
		//Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_resetAxes(this->_devNo );
		
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}		
	}//release dll-accessMutex
}
// ============================================================================
// FpsTaskManager::fpsResetAxis
// ============================================================================ 
void FpsTaskManager::fpsResetAxis(unsigned int axisNo)
{
	if(this->_connected)
	{
		int rc = 0;
		//Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_resetAxis( this->_devNo, axisNo);
	
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}	
	}//release dll-accessMutex
}
// ============================================================================
// FpsTaskManager::fpsSetPosAverage
// ============================================================================ 
void FpsTaskManager::fpsSetPosAverage(unsigned int axisNo, unsigned int sampleTime)
{
	if(this->_connected)
	{
		int rc = 0;
		//Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_setPosAverage( this->_devNo, axisNo, sampleTime);
	
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}	
	}//release dll-accessMutex
}
// ============================================================================
// FpsTaskManager::fpsGetPosAverage
// ============================================================================ 
int FpsTaskManager::fpsGetPosAverage(unsigned int axisNo)
{
	unsigned int sampleTime;
	
	if(this->_connected)
	{
		int rc = 0;
		//Dll access : LockMutex
		yat::MutexLock gard(this->_Fps_Access);
		rc = FPS_getPosAverage( this->_devNo, axisNo, &sampleTime);
	
		std::string message = getMessage(rc);
		if (message != "")
		{
			this->enable_periodic_msg(false);
			this->_errorFromFps = message;
		}	
	}//release dll-accessMutex
	return sampleTime;
}






