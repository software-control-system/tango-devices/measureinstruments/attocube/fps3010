from conan import ConanFile

class FpsInterferometerRecipe(ConanFile):
    name = "fpsinterferometer"
    executable = "ds_FpsInterferometer"
    version = "1.0.13"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Falilou Thiam"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/measureinstruments/attocube/fps3010.git"
    description = "FpsInterferometer device"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        
        self.requires("fps3010/[>=1.0]@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
