#include "fps3010.h"
#include "FpsInterferometer.h"

typedef     int (_stdcall * FuncType_FPS3010_discover)( FPS_InterfaceType ifaces, unsigned int    * devCount );
typedef     int (_stdcall * FuncType_FPS3010_getDeviceInfo)( unsigned int devNo, int * id, char * address, bln32 * connected );
typedef     int (_stdcall * FuncType_FPS3010_connect)( unsigned int devNo);
typedef     int (_stdcall * FuncType_FPS3010_disconnect)( unsigned int devNo);
typedef     int (_stdcall * FuncType_FPS3010_getDeviceConfig)( unsigned int devNo, unsigned int * axisCount, bln32 * syncFtr, bln32 * wlanFtr );
typedef     int (_stdcall * FuncType_FPS3010_getAxisStatus)( unsigned int devNo, unsigned int axisNo, bln32 * valid, bln32 * error );
typedef     int (_stdcall * FuncType_FPS3010_resetAxis)( unsigned int devNo, unsigned int axisNo );
typedef     int (_stdcall * FuncType_FPS3010_resetAxes)( unsigned int devNo);
typedef     int (_stdcall * FuncType_FPS3010_getPosition)( unsigned int devNo, unsigned int axisNo, double     * position );
typedef     int (_stdcall * FuncType_FPS3010_getPositions)( unsigned int devNo, double     * position );
typedef     int (_stdcall * FuncType_FPS3010_setPositionCallback)( unsigned int devNo, FPS_PositionCallback callback, unsigned int lbSmpTime );


HINSTANCE Handle_FPS3010_DLL = NULL;

FARPROC FuncAddr_FPS3010_discover = NULL;
FARPROC FuncAddr_FPS3010_getDeviceInfo = NULL;
FARPROC FuncAddr_FPS3010_connect = NULL;
FARPROC FuncAddr_FPS3010_disconnect = NULL;
FARPROC FuncAddr_FPS3010_getDeviceConfig = NULL;
FARPROC FuncAddr_FPS3010_getAxisStatus = NULL;
FARPROC FuncAddr_FPS3010_resetAxis = NULL;
FARPROC FuncAddr_FPS3010_resetAxes = NULL;
FARPROC FuncAddr_FPS3010_getPosition = NULL;
FARPROC FuncAddr_FPS3010_getPositions = NULL;
FARPROC FuncAddr_FPS3010_setPositionCallback = NULL;

int Initialize_FPS3010_DLL()
{
  if (Handle_FPS3010_DLL == NULL)
  {
    Handle_FPS3010_DLL = LoadLibrary("fps3010.dll");

    if (Handle_FPS3010_DLL != NULL)
    {
      FuncAddr_FPS3010_discover = GetProcAddress(Handle_FPS3010_DLL, "_FPS_discover@8");
      FuncAddr_FPS3010_getDeviceInfo = GetProcAddress(Handle_FPS3010_DLL, "_FPS_getDeviceInfo@16");
      FuncAddr_FPS3010_connect = GetProcAddress(Handle_FPS3010_DLL, "_FPS_connect@4");
      FuncAddr_FPS3010_disconnect = GetProcAddress(Handle_FPS3010_DLL, "_FPS_disconnect@4");
      FuncAddr_FPS3010_getDeviceConfig = GetProcAddress(Handle_FPS3010_DLL, "_FPS_getDeviceConfig@16");
      FuncAddr_FPS3010_getAxisStatus = GetProcAddress(Handle_FPS3010_DLL, "_FPS_getAxisStatus@16");
      FuncAddr_FPS3010_resetAxis = GetProcAddress(Handle_FPS3010_DLL, "_FPS_resetAxis@8");
      FuncAddr_FPS3010_resetAxes = GetProcAddress(Handle_FPS3010_DLL, "_FPS_resetAxes@4");
      FuncAddr_FPS3010_getPosition = GetProcAddress(Handle_FPS3010_DLL, "_FPS_getPosition@12");
      FuncAddr_FPS3010_getPositions = GetProcAddress(Handle_FPS3010_DLL, "_FPS_getPositions@8");
      FuncAddr_FPS3010_setPositionCallback = GetProcAddress(Handle_FPS3010_DLL, "_FPS_setPositionCallback@12");

      return 0;
    }
    else
    {
      return -1;
    }
  }
  else
  {
    return 1;
  }
}

int Finalize_FPS3010_DLL()
{
  if (Handle_FPS3010_DLL != NULL)
  {
    FreeLibrary(Handle_FPS3010_DLL);
    Handle_FPS3010_DLL = NULL;
    FuncAddr_FPS3010_discover = NULL;
    return 0;
  }
  else
    return -1;
}

int FPS_discover( FPS_InterfaceType ifaces, unsigned int    * devCount )
{
  if (FuncAddr_FPS3010_discover != NULL)
  {
    return (*((FuncType_FPS3010_discover) FuncAddr_FPS3010_discover))(ifaces, devCount);
   }
  else
  {
    return -1;
  }
}

int FPS_getDeviceInfo( unsigned int devNo, int * id, char * address, bln32 * connected )
{
  if (FuncAddr_FPS3010_getDeviceInfo != NULL)
  {
    return (*((FuncType_FPS3010_getDeviceInfo) FuncAddr_FPS3010_getDeviceInfo))(devNo, id, address, connected);
   }
  else
  {
    return -1;
  }
}
int FPS_connect( unsigned int devNo )
{
  if (FuncAddr_FPS3010_connect != NULL)
  {
    return (*((FuncType_FPS3010_connect) FuncAddr_FPS3010_connect))(devNo);
   }
  else
  {
    return -1;
  }
}

int FPS_disconnect( unsigned int devNo )
{
  if (FuncAddr_FPS3010_disconnect != NULL)
  {
    return (*((FuncType_FPS3010_disconnect) FuncAddr_FPS3010_disconnect))(devNo);
   }
  else
  {
    return -1;
  }
}

int FPS_getDeviceConfig( unsigned int devNo, unsigned int * axisCount, bln32 * syncFtr, bln32 * wlanFtr )
{
  if (FuncAddr_FPS3010_getDeviceConfig != NULL)
  {
    return (*((FuncType_FPS3010_getDeviceConfig) FuncAddr_FPS3010_getDeviceConfig))(devNo, axisCount, syncFtr, wlanFtr);
   }
  else
  {
    return -1;
  }
}

int FPS_getAxisStatus( unsigned int devNo, unsigned int axisNo, bln32 * valid, bln32 * error )
{
  if (FuncAddr_FPS3010_getAxisStatus != NULL)
  {
    return (*((FuncType_FPS3010_getAxisStatus) FuncAddr_FPS3010_getAxisStatus))(devNo, axisNo, valid, error);
   }
  else
  {
    return -1;
  }
}

int FPS_resetAxis( unsigned int devNo, unsigned int axisNo )
{
  if (FuncAddr_FPS3010_resetAxis != NULL)
  {
    return (*((FuncType_FPS3010_resetAxis) FuncAddr_FPS3010_resetAxis))(devNo, axisNo);
   }
  else
  {
    return -1;
  }
}

int FPS_resetAxes( unsigned int devNo)
{
  if (FuncAddr_FPS3010_resetAxes != NULL)
  {
    return (*((FuncType_FPS3010_resetAxes) FuncAddr_FPS3010_resetAxes))(devNo);
   }
  else
  {
    return -1;
  }
}

int FPS_getPosition( unsigned int devNo, unsigned int axisNo, double     * position )
{
  if (FuncAddr_FPS3010_getPosition != NULL)
  {
    return (*((FuncType_FPS3010_getPosition) FuncAddr_FPS3010_getPosition))(devNo, axisNo, position);
   }
  else
  {
    return -1;
  }
}

int FPS_getPositions( unsigned int devNo, double     * position )
{
  if (FuncAddr_FPS3010_getPositions != NULL)
  {
    return (*((FuncType_FPS3010_getPositions) FuncAddr_FPS3010_getPositions))(devNo, position);
   }
  else
  {
    return -1;
  }
}

int FPS_setPositionCallback( unsigned int devNo, FPS_PositionCallback callback, unsigned int lbSmpTime )
{
  if (FuncAddr_FPS3010_setPositionCallback != NULL)
  {
    return (*((FuncType_FPS3010_setPositionCallback) FuncAddr_FPS3010_setPositionCallback))(devNo, callback, lbSmpTime);
   }
  else
  {
    return -1;
  }
}

